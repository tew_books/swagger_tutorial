![Build Status](https://gitlab.com/pages/gitbook/badges/master/build.svg)

---

TeamEverywhere Javascript Guide
*Updated at 2019-12-01*

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [TeamEverywhere](https://www.team-everywhere.com/)*

- [Swagger란?](#Swagger란?)
- [설치](#설치)
- [schema](#schema)


<!-- END doctoc generated TOC please keep comment here to allow auto update -->


## Swagger란
    - API를 설명하는 문서 작성을 보다 쉽게 해주는 모듈 입니다.
    - Swagger가 더 불편한 거 같다고 생각하시는 분은 엑셀이나 워드로 문서를 공유해보시면 왜 Swagger를 쓰는 지 아실 수 있으실 겁니다!
    - 사이트 : https://swagger.io/

## 설치
    - 처음부터 Swagger를 일일히 설정하며 사용할 수도 있겠지만, 의외로 이를 위해 공부할 것들이 많이 있습니다.
    - 개발자의 삶을 윤택하기 위해 framework를 사용하지만, framework를 사용하기 위해서는 또 공부를 해야 한다는 사실이 고통스러울 수도 있습니다.
    - 많은 주니어 개발자들이 이런 생각을 하기 때문에, 우리는 express를 사용하지 않고 routing을 직접 해봄으로써,
      express가 얼마나 유용한 지 알 수 있었습니다.
    - 그리고 분명히, Swagger도 같은 맥락이라고 저는 생각합니다.
    - npm install --save swagger-ui-express
        - Swagger를 express에서 쉽게 연동할 수 있도록 도와주는 모듈
    - npm install --save swagger-jsdoc
        - Swagger에 필요한 설정을 쉽게 생성할 수 있게 도와주는 모듈

```javascript
    var swaggerUi = require('swagger-ui-express')
    var swaggerJSDoc = require('swagger-jsdoc')
    const swaggerDefinition = {
    info: {
        title: 'School Server API',
        version: '1.0.0',
        description: 'API description',
    },
    host: 'localhost:3000',
    basePath: '/',
    securityDefinitions: {
        bearerAuth: {
        type: 'apiKey',
        name: 'Authorization',
        scheme: 'bearer',
        in: 'header',
        },
    },
    };

    const options = {
        swaggerDefinition,
        apis: ['./routes/*.js'],
    };
    app.get('/swagger.json', (req, res) => {
        res.setHeader('Content-Type', 'application/json');
        res.send(swaggerSpec);
    });

    app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

```    
  
## schema
    - 각 파라미터들의 조건을 얘기합니다.    
    - YAML이라고 하는 데이터 포맷을 따릅니다.
    - 주요 들어가는 정보는 아래와 같습니다.
        - tags : Swagger에서의 API 카테고리
        - parameters의 name : 서버에 보낼 데이터 오브젝트의 키로 우리가 서버에서 req.body 등으로 받는 부분에 넣는 데이터
```javascript 
        /**
         * @swagger
         * /school:
         *   post:
         *     tags:
         *       - Schools
         *     name: Register School
         *     summary: Register School         
         *     parameters:
         *       - name: body
         *         in: body
         *         schema:           
         *           type: object
         *           properties:
         *             name: 
         *               type: string
         *             type:
         *               type: string
         *             address:
         *               type: string
         *           example:
         *             name: schoolOne
         *             type : High
         *             address : gangnam
         *                 
         *     responses:
         *       '200':
         *         description: Register one Schhol
         *       '404':
         *         fail 
        
         *   get:
         *     tags:
         *       - Schools
         *     name: Get School
         *     summary: Get School
         *     parameters:
         *       - in: query
         *         name: schoolName         
         *         schema:           
         *           type: object
         *           properties:
         *             name: 
         *               type: string
         *     responses:
         *       '200':
         *         description: Get one Schhol
         *       '404':
         *         fail
         */
```
            